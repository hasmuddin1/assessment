import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Input from '../../components/Input'
import { getImge } from '../../utils/getImage'
import { GoBack } from '../../components/GoBack'
import { EVENT_CREATE, initState } from './constants'


export const Create = () => {
    const [form, setForm] = useState(initState)
    const [errors, setError] = useState(initState)
    const router = useNavigate()

    const handleChange = (e) => {
        const { name, value } = e.target
        setForm(pre => ({
            ...pre,
            [name]: value
        }))
        if (errors[name]) {
            setError(pre => ({
                ...pre,
                [name]: false
            }))
        }
    }

    const handleBlur = (e) => {
        const { name, value } = e.target
        setError(pre => ({
            ...pre,
            [name]: !value
        }))
    }

    const onSubmit = (e) => {
        e.preventDefault()
        const isValid = Object.keys(form).every(i => !!form[i])
        if (!isValid) {
            const errorData = Object.keys(form).reduce((acc, item) => {
                if (!form[item]) {
                    acc[item] = true
                }
                return acc
            }, {})
            setError(errorData)
            return null
        }
        localStorage.setItem('data', JSON.stringify(form))
        router('/event')
    }

    return (
        <>
        <GoBack to='/'/>
        <div className="pl-20 pr-40">
            <h4 className='text-center mt-4 mb-4 text-4xl text-skin-primary font-bold'>Create Event</h4>
            <div className="max-w-full flex justify-center pb-10 pt-3 flex mx-auto overflow-hidden md:max-w-full">
                <div className="w-6/12">
                    <img src={getImge('create')} alt='create' className='h-full' />
                </div>
                <div className='w-6/12'>
                    <form onSubmit={onSubmit}>
                        {EVENT_CREATE.map(props => (
                            <Input {...{ ...props, key: props.name, onChange: handleChange, handleBlur, value: form[props.name], error: errors[props.name] }} />
                        ))}
                        <div className='flex justify-end my-10'>
                            <button type='submit' className="bg-gradient-primary items-center align-center flex justify-center text-center font-bold text-xl text-white px-2 py-1 w-80 h-14 rounded-primary">
                                <span>Next</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}

export default Create