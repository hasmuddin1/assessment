export const EVENT_CREATE = [{
    name: 'host',
    label: 'Host Name',
},
{
    name: 'event',
    label: 'Event Name',
},
{
    name: 'address',
    label: 'Address',
},
{
    name: 'start',
    label: 'Start Date',
    type: 'datetime-local'
},
{
    name: 'end',
    label: 'End Date',
    type: 'datetime-local'
}
]


export const initState = EVENT_CREATE.map(k => k.name).reduce((acc, i) => {
    acc[i] = ''
    return acc
}, {})