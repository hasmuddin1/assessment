import React, { useEffect, useState } from 'react'
import { VscCalendar } from 'react-icons/vsc';
import { GoLocation } from 'react-icons/go'
import IconRow from '../../components/IconRow';
import { getTimeWithDate } from '../../utils/getTimeWithDate';
import { getImge } from '../../utils/getImage';
import { GoBack } from '../../components/GoBack';
import { useNavigate } from 'react-router-dom';
import { initState } from '../Create/constants';


const getEvent = () => {
    const data = localStorage.getItem('data')
    if (data) {
        return JSON.parse(data)
    }
    return {}
}

export const Event = () => {
    const [editable, setEditable] = useState(initState)
    const {
        host,
        address,
        start,
        end,
        event
    } = getEvent()
    const router = useNavigate()


    useEffect(() => {
        if (!host) {
            router('/')
        }
    }, [host, router])


    const onDbClick = (e, name)=>{
        setEditable(pr=>({
            ...pr,
            [name]:true
        }))
    }

    return (
        <>
            <GoBack to='/' />
            <div className="px-72 py-12">
                <div className="max-w-full py-10 flex mx-auto overflow-hidden md:max-w-full justify-between">
                    <div>
                        <div className="flex items-start flex-col h-full justify-start p-10">
                            <span className='text-5xl text-skin-primary font-bold'contentEditable={!!editable.event} onDoubleClick={(e)=>onDbClick(e, 'event')}>{event}</span>
                            <span className='text-skin-secondary mt-2' contentEditable={!!editable.host}>Hoisted by : <span className='font-bold'>{host}</span></span>
                            <div className="d-flex justify-start w-full mt-4">
                                <IconRow {...{
                                    icon: GoLocation,
                                    header: getTimeWithDate(start),
                                    subHeader: <span>to <span className="font-medium">{getTimeWithDate(end)}</span></span>,
                                }} />
                                <IconRow {...{
                                    icon: VscCalendar,
                                    header: "Address",
                                    subHeader: address,
                                }} />
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <div className="md:shrink-0">
                        <img className="h-80 w-full object-cover md:h-full md:w-80" src={getImge('event')} alt="event" />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Event