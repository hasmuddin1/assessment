import React from 'react'
import { convertToCss } from '../../utils/convertToCss'

export const Input = ({ label, name, value, onChange, handleBlur, type = 'text', error }) => {
    return (
        <label htmlFor={name} className="block mb-6">
            <span className="block text-lg font-medium text-slate-700 mb-1">{label}</span>
            <input onBlur={handleBlur} required autoFocus={name === 'host'} type={type} onChange={onChange} id={name} name={name} value={value} className={`border rounded-lg placeholder-slate-400 w-full py-2 focus-visible:outline-primary valid:border-green-500
             ${convertToCss({
                'px-4': type === 'text',
                "px-1": type !== 'text',
                'invalid:border-red-500': error
            })}`
            } />
           {error && <span className="text-red-400">Required field</span>}
        </label>
    )
}

export default Input