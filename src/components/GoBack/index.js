import React from 'react'
import { IoIosArrowBack } from 'react-icons/io'
import { useNavigate } from 'react-router-dom'

export const GoBack = ({ to }) => {
    const navigate = useNavigate()
    return (
        <span onClick={() => navigate(to)} className='flex items-center mt-3 ml-3 cursor-pointer'>
            <IoIosArrowBack /><span>Home</span>
        </span>
    )
}
