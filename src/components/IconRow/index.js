import React from 'react'
import { IoIosArrowForward } from 'react-icons/io'

const IconRow = ({icon:Icon, header, subHeader}) => {
  return (
    <main className='flex justify-between w-full items-center'>
          <section className='flex items-center space-x-3 py-3 w-full mr-10'>
          <aside className='bg-white border w-10 h-10 flex rounded-lg items-center justify-center'>
              <Icon fill="#240D57" />
          </aside>
          <aside>
              <span className='block text-skin-primary font-bold'>{header}</span>
              <span>{subHeader}</span>
          </aside>
          </section>
          <IoIosArrowForward />
      </main>
  )
}

export default IconRow