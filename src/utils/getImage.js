export const getImge = (name) => {
    const image =  {
        event:"https://s3.us-west-2.amazonaws.com/secure.notion-static.com/17d6299f-f287-469c-a403-b8ab9d75aa62/Birthday_cake.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20221001%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20221001T042746Z&X-Amz-Expires=86400&X-Amz-Signature=4343d1110c00e506b04c177afa8bb7e8717ad0080a04d23695bc964b23f09292&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Birthday%2520cake.png%22&x-id=GetObject",
        home:'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/ea95af2d-7f06-4f25-859c-9069519053a7/Landing_page_image.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20221001%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20221001T042623Z&X-Amz-Expires=86400&X-Amz-Signature=d2f8e4f48ceee4528990790624635b76ae3cbce2dfb6fa443539584ff0ec17a7&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Landing%2520page%2520image.svg%22&x-id=GetObject',
        create:'https://img.freepik.com/free-vector/mobile-login-concept-illustration_114360-83.jpg?w=1060&t=st=1664611029~exp=1664611629~hmac=47ba2de2e098bc76e04e6ee3ca0c36bd40ebb73db57ad1356a19e49393229a69'
    }
    return image[name]
}