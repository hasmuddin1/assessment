export const getTimeWithDate = (time)=>{
    const myTime =  new Date(time)
    return `${myTime.toDateString()}, ${myTime.toLocaleTimeString()}`
}