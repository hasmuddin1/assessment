export const convertToCss = (object) => {
    return Object.keys(object).filter(Boolean).reduce((acc, curr) => {
        if (object[curr]) {
            acc += curr + " "
        }
        return acc
    }, "")
}